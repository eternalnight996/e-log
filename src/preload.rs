#[cfg(feature = "dialog")]
pub use super::dialog::*;
pub use super::{debug, debug2, error, error2, info, info2, trace, trace2, warn, warn2};
pub use super::{Level, LevelFilter, LogTarget};
